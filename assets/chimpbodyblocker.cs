using Godot;
using System;

public class chimpbodyblocker : Path
{
    [Export] public float speed = 1f;
    private PathFollow pathFollow;

    public override void _Ready()
    {
        pathFollow = GetChild<PathFollow>(0);
    }

    public override void _PhysicsProcess(float dt)
    {
        pathFollow.Offset += speed*dt;
    }
}
