using Godot;
using System;
using gombegolf;

namespace gombegolf
{

    public class DevSwingControl : Spatial
    {
        private SwingControl _swingControl;
        private Banana _banana;
        private Spatial _bananaContainer;

        public override void _Ready()
        {
            _swingControl = GetNode<SwingControl>("%SwingControl");
            _banana = GetNode<Banana>("%Banana");
            _bananaContainer = GetNode<Spatial>("%BananaContainer");

            _banana.Connect("sleeping_state_changed", this, "OnBananaSleepingStateChanged");
            
            // Listen to the SwingConfirmed event. Apply banana force on fire
            _swingControl.SwingConfirmed += _OnSwingConfirmed;
        }

        private void _InitSwingControl()
        {
            _swingControl = new SwingControl();
            _swingControl.SwingConfirmed += _OnSwingConfirmed;
            _bananaContainer.AddChild(_swingControl);
        }

        private void _ClearSwingControl()
        {
            _swingControl.QueueFree();
            _swingControl = null;
        }

        private void _OnSwingConfirmed(object sender, SwingControl.SwingConfirmedEventArgs e)
        {
            // TODO: Spin
            var dt = 1f / Engine.IterationsPerSecond;
            // Transform the hit direction out of the SwingControl's basis first
            // (in case we're on an incline or something).
            var hitDir = _swingControl.Transform.basis.Xform(e.Swing.HitDirection);
            _banana.AddCentralForce(e.Swing.Power * e.Swing.HitDirection / dt);
            
            _ClearSwingControl();
        }

        public void OnBananaSleepingStateChanged()
        {
            // TODO: This is where we want to do transitions and enable/disable SwingControl.
            if (_banana.Sleeping && _swingControl == null) _InitSwingControl();
        }

        public override void _PhysicsProcess(float dt)
        {
            if (_swingControl != null)
            {
                _swingControl.Translation = _banana.Translation;
            }
        }

        public override void _Process(float dt)
        {
        }
    }

}