extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	for ba in $B1.get_children():
		var axis = ba.global_transform.basis.xform(Vector3.UP)
		ba.add_torque(500*axis)
		
	for ba in $B2.get_children():
		var axis = ba.global_transform.basis.xform(Vector3.UP)
		ba.add_torque(500*axis)
