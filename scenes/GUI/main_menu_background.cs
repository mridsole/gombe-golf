using Godot;
using System;

namespace gombegolf
{
    public class main_menu_background : Spatial
    {
        public override void _Ready()
        {
            Music music = (Music)GetNode("/root/Music");
            music.play_song(Resources.MenuThemeMusic);
            var menu = GetNode<Control>("Menu");
            var volumeSlider = menu.GetNode<Slider>("%Volume");
            volumeSlider.Connect("value_changed", this, "_OnVolumeChanged");
            //GetTree().CallGroup("chimp_animation_tree", "play_animation", "FistJump-loop", -1, 10 );
        }

        private void _OnVolumeChanged(float vol)
        {
            // Adjust all chimp ears.
            foreach (Spatial chimp in GetNode("chimps").GetChildren())
            {
                chimp.GetNode<MeshInstance>("Armature/Skeleton/Model").Callv("set_ears", new Godot.Collections.Array(vol));
            }
        }
    }
}
