extends TextureButton

var default_icon = preload("res://scenes/levels/default.png")
var level_path setget set_level_path

func set_level_path(value):
	var dir = value.get_base_dir()+"/"
	name = value.get_basename().substr(dir.length())
	var icon = load(dir+name+".png")
	if !icon:
		icon = default_icon
	texture_normal = icon
	#	# $HighScore.text = Persistence["Highscores"][name]["total"]
	#	# if no highscore/name N/A
	get_node("%level_name").text = name
	connect("button_up", SceneManager, "goto_scene", [value])
	level_path = value
