extends HFlowContainer

var icon_scene = preload("res://scenes/GUI/level_icon.tscn")
var level_folder = "res://scenes/levels/"

func _ready():
	var dir = Directory.new()
	dir.open(level_folder)
	dir.list_dir_begin()
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif not file.begins_with("."):
			if file.ends_with(".tscn"):
				var level_icon = icon_scene.instance()
				level_icon.level_path = level_folder+file
				add_child(level_icon)
				level_icon.set_owner(get_tree().edited_scene_root)
	dir.list_dir_end()

