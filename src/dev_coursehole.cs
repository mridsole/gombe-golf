using Godot;
using System;
using gombegolf;

public class dev_coursehole : Spatial
{
    public override void _Ready()
    {
        var holes = GetNode<Spatial>("%Holes");
        foreach (var hole in holes.GetChildren())
        {
            var courseHole = hole as CourseHole;
            courseHole.Begin(this.GetNode<CameraRig>("%CameraRig"));
        }
    }
}
