extends Control

func _ready():
	var pause_menu = get_node("/root/PauseMenu")
	pause_menu.CanPause = false
	pause_menu.Paused = false
	$"%Volume".connect("value_changed", self, "change_volume")
	$"%Volume".value = .5
	$"%QuitButton".connect("pressed", self, "on_quit_pressed")
	$"%PlayButton".connect("pressed", self, "on_play_pressed")

func change_volume(value):
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), linear2db(value))
	# TODO TODO TODO: CHIMP EARS


func on_quit_pressed():
	get_tree().quit();

func on_play_pressed():
	get_tree().change_scene("res://scenes/MainCourseWrapper.tscn")
