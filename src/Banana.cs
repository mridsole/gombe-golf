using Godot;
using System;
using System.Drawing.Design;
using Array = Godot.Collections.Array;

namespace gombegolf
{
	/// This adds some custom physics-y stuff on top of RigidBody:
	/// 
	///		1. More aggressive sleeping via:
	///			- LinearVelocitySleepThreshold
	///			- AngularVelocitySleepThreshold
	///			- SleepThresholdTime
	/// 
	///		2. Particle effects when the banana hits a wall (we use a custom integrator to save the prev velocity)
	///
	///		3. TODO: Boomerang-style spin dynamics?
	///
	///		4. Non-linear damping. Slow it down more when it's going slower.
	/// 
	public class Banana : RigidBody
	{
		static private PackedScene BananaHitParticles = GD.Load<PackedScene>("res://assets/particles/Banana_hit.tscn");
		static private Array ImpactSounds = new Array(
			Resources.Impact1Sound,
			Resources.Impact2Sound,
			Resources.Impact3Sound,
			Resources.Impact4Sound
		);

		[Export] public float LinearVelocitySleepThreshold = 0.90f;
		[Export] public float AngularVelocitySleepThreshold = 0.90f;
		[Export] public float SleepThresholdTime = 0.5f;
		[Export] public float SplatLinearVelocityThreshold = 3f;
		[Export] public float SplatAngularVelocityThreshold = 3f;
		[Export] public float SpinFactor = 0.5f;

		public float TimeUnderVelocityThresholds = 0.0f;

		public Vector3 PreviousLinearVelocity;
		public Vector3 PreviousAngularVelocity;

		// would like to do this in a collection of some kind but w.e
		private Godot.Collections.Array<Particles> _bananaHitParticles;
		private int MaxParticle = 3;
		
		public AudioStreamPlayer3D ImpactAudio;
		
		public override void _Ready()
		{
			_bananaHitParticles = new Godot.Collections.Array<Particles>();
			this.Connect("sleeping_state_changed", this, "_OnSleepingStateChanged");
			this.Connect("body_entered", this, "_OnBodyEntered");
            for (int i = 0; i < MaxParticle; i++)
            {
				var instance = BananaHitParticles.Instance<Particles>();
				instance.OneShot = true;
				instance.Emitting = false;
				_bananaHitParticles.Add(instance);
				instance.SetAsToplevel(true);
				AddChild(instance);
			}
            ImpactAudio = GetNode<AudioStreamPlayer3D>("ImpactAudio");
		}

		public void PlayHitParticles()
		{
            var rand = new Random();
            ImpactAudio.Stream = (AudioStream)ImpactSounds[rand.Next(ImpactSounds.Count)];
            ImpactAudio.OutOfRangeMode = AudioStreamPlayer3D.OutOfRangeModeEnum.Mix;
            ImpactAudio.Seek(0);
            ImpactAudio.Play();
            GD.Print("Playing ", ImpactAudio.Stream.ResourceName);
            
            foreach (var item in _bananaHitParticles)
            {
                if (!item.Emitting)
                {
					item.OneShot = true;
					item.Emitting = true;
					item.Translation = this.GlobalTransform.origin;
					return;
				}
            }
		}

		public override void _IntegrateForces(PhysicsDirectBodyState state)
		{
			// Save these so we know how fast it was going when we collide with something.
			PreviousLinearVelocity = LinearVelocity;
			PreviousAngularVelocity = AngularVelocity;
			base._IntegrateForces(state);
		}

		public override void _PhysicsProcess(float dt)
		{
			var linvel = LinearVelocity.Length();
			var angvel = AngularVelocity.Length();
			var underVelThresholds = linvel < LinearVelocitySleepThreshold && angvel < AngularVelocitySleepThreshold;
			if (underVelThresholds)
				TimeUnderVelocityThresholds += dt;
			else
				TimeUnderVelocityThresholds = 0f;

			if (TimeUnderVelocityThresholds > SleepThresholdTime && !Sleeping)
				Sleeping = true;
			
			// Boomerang spin. Only process if above velocity thresholds. (avoid preventing sleep)
			if (!underVelThresholds)
			{
				var spinAngularVelocity = AngularVelocity.Project(GlobalTransform.basis.XformInv(Vector3.Up));
				var spinForce = -SpinFactor * LinearVelocity.Cross(spinAngularVelocity);
				AddCentralForce(spinForce);
			}
		}

		/// NOTE: This is a global vector!
		public Vector3 GetPlaySurfaceNormal()
		{
			// Idea: shoot a ray downwards from the COM of banana (exclude the banana ofc).
			var spaceState = GetWorld().DirectSpaceState;
			var from = GlobalTransform.origin;
			var to = from + 2f * (new Vector3(0f, -1f, 0f));
			var rayRes = spaceState.IntersectRay(from, to, new Array(this), 1, true, true);
			if (rayRes != null && rayRes.Contains("normal"))
				return (Vector3)rayRes["normal"];
			else
				return Vector3.Up; // No surface found below, just return global "up"
		}

		public void _OnBodyEntered(Node other)
		{
			if (
				PreviousLinearVelocity.Length() > SplatLinearVelocityThreshold ||
				PreviousAngularVelocity.Length() > SplatAngularVelocityThreshold
			)
			{
				// TODO: Play the effect
				PlayHitParticles();
			}
		}

		public void _OnSleepingStateChanged()
		{
			if (!Sleeping) TimeUnderVelocityThresholds = 0f;
		}
	}
}