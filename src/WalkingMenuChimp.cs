using Godot;
using System;

namespace gombegolf
{
    public class WalkingMenuChimp : Spatial
    {
        public float t = 0f;

        public override void _Ready()
        {

        }

        public override void _Process(float dt)
        {
            TranslateObjectLocal(5f * dt * new Vector3(0f, 0f, 1f));
            t += dt;
            if (t > 12f)
            {
                RotateObjectLocal(new Vector3(0, 1, 0), Mathf.Pi);
                t = 0f;
            }
        }
    }
}