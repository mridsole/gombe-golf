using Godot;
using System;
using System.Drawing.Design;

public class BananaSoft : SoftBody
{
    public enum BananaState
    {
        Inactive,       // Other players turn? TODO
        PendingShot,    // Waiting for player to shoot.
        InMotion        // Banana in the air
    }

    [Export] public BananaState State = BananaState.Inactive;

    private Camera _camera;
    private ImmediateGeometry _imGeom = new ImmediateGeometry();
    
    public override void _Ready()
    {
        // Testing
        State = BananaState.PendingShot;
        
        // HACK
        _camera = GetParent().GetParent().GetNode<Camera>("Camera");
        AddChild(_imGeom);
    }

    public override void _PhysicsProcess(float dt)
    {
        var mousePos = GetViewport().GetMousePosition();
       
        // Ray-cast mouse to banana collider, get contact point.
        var spaceState = GetWorld().DirectSpaceState;
        var from = _camera.ProjectRayOrigin(mousePos);
        var to = from + 100 * _camera.ProjectRayNormal(mousePos);
        var rayRes = spaceState.IntersectRay(from, to, null,  1, true, true);

        _imGeom.Clear();
        if (
            rayRes != null &&
            rayRes.Contains("position") &&
            rayRes.Contains("normal")
            )
        {
            var hit = (Vector3)rayRes["position"];
            var norm = (Vector3)rayRes["normal"];
            var hitLocalStart = _imGeom.GlobalTransform.XformInv(hit);
            var hitLocalEnd = _imGeom.GlobalTransform.XformInv(hit + 10f * norm);
            _imGeom.Begin(Mesh.PrimitiveType.Lines, null);
            _imGeom.AddVertex(hitLocalStart);
            _imGeom.AddVertex(hitLocalEnd);
            _imGeom.End();

            if (Input.IsActionJustPressed("hit_banana"))
            {
                var rb = ((Area)rayRes["collider"]).GetParent();
                var body = (SoftBody)((Area)rayRes["collider"]).GetParent();
            }
        }
    }
}
