using Godot;
using System;

namespace gombegolf
{

    public class DevCourse : Spatial
    {
        public override void _Ready()
        {
            var course = GetNode<Course>("Course");
            course.Begin();
        }
    }
}