using Godot;
using System;
using Array = Godot.Collections.Array;

namespace gombegolf
{

    /// Gameplay logic for a single hole.
    /// This expects the following node structure (see example scenes):
    ///     Contents: Spatial           - The geometry of the course (meshes, colliders etc)
    ///     Start: Spatial              - Position defines where the banana starts
    ///     EndArea: Area               - Win condition region (region should cover the hole)
    ///     OutOfBoundsArea: Area       - If banana touches this area, reset shot
    /// 
    /// TODO: Support for multiple players.
    ///
    /// Top-level states:
    ///     1. Intro (some sort of intro cutscene? with commentators?)
    ///     2. Aim
    ///     3. Banana flying
    ///     4. HoleReview
    public class CourseHole : Spatial
    {
        public enum HoleState { Inactive, Intro, Aim, Swinging, BananaFlying, HoleReview };
        
        [Export] public float PreviewDistance = 50f;
        [Export] public int Par = 3;
        [Export] public int SkipShots = 99;
        [Export] public bool SkipIntro = false;
        
        // Initial nodes.
        public Spatial Start;
        public Area EndArea;
        public Area OutOfBoundsArea;
        
        public HoleState State = HoleState.Inactive;
        public Banana Banana;
        public Banana GhostBanana;
        public Spatial Chimp;
        public Spatial ChimpPivot;
        public SwingControl SwingControl;
        public SwingControl.SwingParams PendingSwing;
        public Timer SwingingTimer = new Timer();
        public SceneTreeTween IntroTween = null;
        public SceneTreeTween WinTween = null;

        private CameraRig _cameraRig;
        private Transform _lastShotPose;
        
        private Vector3 _localPlaySurfaceNormal;

        public int ShotsTaken;
        
        public class HoleResult : EventArgs { public int ShotsTaken; }
        public event EventHandler<HoleResult> HoleComplete;

        private Music music;
        private AudioStreamPlayer _angryChimpPlayer;
        
        public override void _Ready()
        {
            // NOTE: No need for % cos all these are required to be top-level anyway.
            Start = GetNode<Spatial>("Start");
            EndArea = GetNode<Area>("EndArea");
            OutOfBoundsArea = GetNode<Area>("OutOfBoundsArea");
            SwingControl = new SwingControl();
            SwingControl.SwingConfirmed += _OnSwingConfirmed;
            SwingControl.GhostHitEvent += _OnGhostHitEvent;
            SwingControl.GhostWakeupEvent += _OnGhostWakeupEvent;
            music = GetNode<Music>("/root/Music");
            AddChild(SwingingTimer);
            SwingingTimer.Connect("timeout", this, "_IncrementShots");
            _angryChimpPlayer = new AudioStreamPlayer();
            AddChild(_angryChimpPlayer);
        }

        public void Begin(CameraRig cameraRig)
        {
            _cameraRig = cameraRig;
            // _TweenCameraToCourseStart();
            ShotsTaken = 0;
            State = HoleState.Intro;
            
            Chimp = Resources.ChimpScene.Instance<Spatial>();
            ChimpPivot = new Spatial();
            ChimpPivot.AddChild(Chimp);
            Chimp.Translation = new Vector3(-0.35f, -0.1f, -1.4f);
            AddChild(ChimpPivot);

            ChimpPivot.Transform = Start.Transform;
            ChimpPivot.RotateObjectLocal(Vector3.Up, -Mathf.Pi/2);
            SwingControl.Swing.HorizontalAngle = ChimpPivot.Rotation.y;
           _SyncPivotAim();
           
            if (!SkipIntro) _CreateIntroSequenceTween();
            _cameraRig.Active = false;
            
            _PlayIntroMusic();
        }

        private bool _IntroComplete() { return IntroTween == null || !IntroTween.IsRunning(); }

        private void _CreateIntroSequenceTween()
        {

            IntroTween = GetTree().CreateTween();
            IntroTween
                .TweenMethod(this, "_IntroSequence_TweenCameraToMiddle", 0f, 1f, 1f, new Array(_cameraRig.Translation, _cameraRig.Latitude, _cameraRig.Longitude, _cameraRig.Distance))
                .SetEase(Tween.EaseType.Out).SetTrans(Tween.TransitionType.Expo);
            IntroTween
                .Parallel()
                .TweenMethod(this, "_IntroSequence_TweenCamera360", 0f, 2f * Mathf.Pi, 4.6f)
                .SetEase(Tween.EaseType.InOut).SetTrans(Tween.TransitionType.Sine);
            IntroTween
                .TweenMethod(this, "_IntroSequence_TweenCameraToStart", 0f, 1f, 1.5f)
                .SetEase(Tween.EaseType.InOut).SetTrans(Tween.TransitionType.Sine);
        }

        private void _IntroSequence_TweenCameraToMiddle(float t, Vector3 initPos, float initLat, float initLong, float initDist)
        {
            var finalPos = Transform.Xform(Vector3.Zero);
            _cameraRig.Translation = initPos.LinearInterpolate(finalPos, t);
            _cameraRig.Latitude = Mathf.Lerp(initLat, Mathf.Deg2Rad(60), t);
            _cameraRig.Longitude = Mathf.Lerp(initLat, 0, t);
            _cameraRig.Distance = Mathf.Lerp(initDist, PreviewDistance, t);
        }

        private void _IntroSequence_TweenCamera360(float angle)
        {
            _cameraRig.Longitude = angle;
        }
        
        private void _IntroSequence_TweenCameraToStart(float t)
        {
            var initPos = Transform.Xform(Vector3.Zero);
            var finalPos = Transform.Xform(Start.Translation);
            _cameraRig.Translation = initPos.LinearInterpolate(finalPos, t);
            _cameraRig.Distance = Mathf.Lerp(PreviewDistance, 15f, t);
        }

        private void _CreateWinSequenceTween()
        {
            // Just delay...?
            WinTween = GetTree().CreateTween();
            // HACK: BAD don't use tween for this dummy
            WinTween.TweenProperty(this, "PreviewDistance", PreviewDistance, 0.1f).SetDelay(3f);
        }

        private void _CompleteHole()
        {
            // TODO: We want to set a timer eventually, so we can have the chimp celebrate for a bit
            HoleResult args = new HoleResult();
            args.ShotsTaken = ShotsTaken;
            HoleComplete?.Invoke(this, args);
            State = HoleState.Inactive;
            RemoveChild(Banana);
        }

        public void End()
        {
            if (ChimpPivot.GetParent() != null) ChimpPivot.GetParent().RemoveChild(ChimpPivot);
            music.Stop();
            State = HoleState.Inactive;
        }

        public void Deactivate()
        {
            State = HoleState.Inactive;
        }

        public override void _Input(InputEvent @event)
        {
            if (State == HoleState.Inactive || State == HoleState.Intro || State == HoleState.HoleReview) return;
            if (@event.IsActionReleased("reset"))
            {
                SwingingTimer.Stop();
                _BananaSetup();
                _ToAim();
            }
            else if (@event.IsActionReleased("skip"))
            {
                ShotsTaken = SkipShots;
                _CompleteHole();
            }
        }

        public override void _PhysicsProcess(float dt)
        {
            if (State == HoleState.Inactive)
            {
                // Transition commanded from parent.
            }
            else if (State == HoleState.Intro)
            {
                // Once the intro cutscene's done, move on.
                if (_IntroComplete()) _IntroToAim();
            }
            else if (State == HoleState.Aim)
            {
                // Sync chimp rotation with aim
                _SyncPivotAim();
                // Transition handled in `_OnSwingConfirmed`
            }
            else if (State == HoleState.Swinging)
            {
                if (SwingingTimer.TimeLeft <= 0)
                {
                    _ToBananaFlying();
                }
            }
            else if (State == HoleState.BananaFlying)
            {
                // TODO: Do we want this?
                // Bind camera to banana.
                _cameraRig.Translation = Transform.Xform(Banana.Translation);
                
                // Check for win condition.
                if (EndArea.OverlapsBody(Banana))
                {
                    _ToWinHoleReview();
                }
                else if (OutOfBoundsArea.OverlapsBody(Banana))
                {
                    _PlayAngryChimp();
                    _ResetToLastShot(Banana);
                    _ToAim();
                }
                // NOTE: Transition to Aim also handled in `_OnBananaSleepingStateChanged`
            }
            else if (State == HoleState.HoleReview)
            {
                if (!WinTween.IsRunning()) _CompleteHole();
            }
            else
            {
                throw new Exception("Unknown CourseHole state");
            }
        }

        private void _ResetToLastShot(Banana Banana)
        {
            Banana.Transform = _lastShotPose;
            Banana.LinearVelocity = Vector3.Zero;
            Banana.AngularVelocity = Vector3.Zero;
            Banana.Sleeping = true;
        }

        private void _IntroToAim()
        {
            _cameraRig.Active = true;
            // Set up the banana at the start.
            Banana = Resources.BananaScene.Instance<Banana>();
            GhostBanana = Resources.GhostBananaScene.Instance<Banana>();
            AddChild(Banana);
            _BananaSetup();
            Banana.Connect("sleeping_state_changed", this, "_OnBananaSleepingStateChanged");
            _ToAim();
        }

        private void _BananaSetup()
        {
            Banana.Rotation = Start.Rotation;
            Banana.Translation = Start.Translation;
            Banana.LinearVelocity = Vector3.Zero;
            Banana.AngularVelocity = Vector3.Zero;
            Banana.TranslateObjectLocal(new Vector3(0f, 0.2f, 0f));
            Banana.Sleeping = true;
        }

        private void _ToAim()
        {
            _lastShotPose = Banana.Transform;
            var atree = Chimp.GetNode<AnimationTree>("AnimationTree");
            atree.Callv("play_animation", new Array("Swing-Idle", 0));
            _TweenCameraToBanana();
            _EnableSwingControl();
            SwingControl.Translation = Banana.Translation;
            var playSurfaceNormal = Banana.GetPlaySurfaceNormal();
            _localPlaySurfaceNormal = Transform.basis.XformInv(playSurfaceNormal);
            // var swingControlAxis = _localPlaySurfaceNormal.Cross(SwingControl.Transform.basis.Xform(Vector3.Up)).Normalized();
            var swingControlAxis = _localPlaySurfaceNormal.Cross(Vector3.Up).Normalized();
            if (Mathf.IsEqualApprox(swingControlAxis.Length(), 1f))
            {
                SwingControl.Rotation = Vector3.Zero;
                SwingControl.Rotate(swingControlAxis, -_localPlaySurfaceNormal.SignedAngleTo(Vector3.Up, swingControlAxis));
            }
            
            ChimpPivot.Translation = Banana.Translation;
            
            if (GhostBanana.GetParent() == null) AddChild(GhostBanana);
            
            _PlayAimMusic();
            State = HoleState.Aim;
        }

        private void _ToWinHoleReview()
        {
            var confetti = Resources.WinConfettiScene.Instance<Particles>();
            AddChild(confetti);
            confetti.Translation = Banana.Translation;
            confetti.OneShot = true;

            _PlayWinMusic();
            
            // TODO: Rotate chimp to face end?
            var atree = Chimp.GetNode<AnimationTree>("AnimationTree");
            atree.Callv("play_animation", new Array("Jump-loop", 0));
            
            _CreateWinSequenceTween();
            
            State = HoleState.HoleReview;
        }

        // When we go out of bounds
        private void _PlayAngryChimp()
        {
            _angryChimpPlayer.Stream = Resources.ChimpAngrySound;
            _angryChimpPlayer.Seek(0);
            _angryChimpPlayer.Play();
        }

        private void _PlayIntroMusic()
        {
            music.play_song(Resources.GenralDrumsMusic);
        }

        private void _PlayAimMusic()
        {
            music.play_song(Resources.AimMusic);
        }

        private void _PlayBananaFlyingMusic()
        {
            music.play_song(Resources.BananaFlyingMusic);
        }

        private void _PlayWinMusic()
        {
            // TODO: Actual win song
            music.play_song(Resources.WinMusic);
        }

        private void _EnableSwingControl()
        {
            // Enable/disable by adding to or removing from the scene.
            // It belongs in the banana container.
            //
            if (!SwingControl.IsInsideTree()) AddChild(SwingControl);
        }

        private void _DisableSwingControl()
        {
            if (SwingControl.IsInsideTree()) RemoveChild(SwingControl);
        }

        private void _OnSwingConfirmed(object sender, SwingControl.SwingConfirmedEventArgs e)
        {
            // XXX: We need to wake it up with this first, otherwise the torque impulse is applied incorrectly!
            Banana.Sleeping = false;
            
            if (GhostBanana.GetParent() != null) GhostBanana.GetParent().RemoveChild(GhostBanana);
            
            _DisableSwingControl();
            State = HoleState.Swinging;
            _lastShotPose = Banana.Transform;
            SwingingTimer.OneShot = true;
            SwingingTimer.Autostart = false;
            // TODO: Sketchy... instead of timer, sync with anim keyframe or something?
            SwingingTimer.Start(0.7f);
            
            PendingSwing = e.Swing;
            
            var atree = Chimp.GetNode<AnimationTree>("AnimationTree");
            atree.Callv("play_animation", new Array("Swing",0));
        }

        private void _OnGhostHitEvent(object sender, SwingControl.SwingConfirmedEventArgs e)
        {
            //setting this everytime so i dont have to make another event
            GhostBanana.Visible = true;
            GhostBanana.PauseMode = PauseModeEnum.Inherit;
            // I would rather send the swing then set it 
            PendingSwing = e.Swing;
            GhostBanana.Sleeping = false;
            _ResetToLastShot(GhostBanana);
            _HitBanana(GhostBanana);
        }

        private void _OnGhostWakeupEvent(object sender, object e)
        {
            // WARNING e I S NULL WARNING!!
            e = null;
            GhostBanana.Visible = false;
            GhostBanana.PauseMode = PauseModeEnum.Stop;
        }

        private void _ToBananaFlying()
        {
            State = HoleState.BananaFlying;
            _PlayBananaFlyingMusic();
            // Splat
            if (Banana.GetParent() != null) Banana.PlayHitParticles();
            // Force the banana (with spin).
            _HitBanana(Banana);
        }

        private void _HitBanana(Banana banana)
        {
            var dt = 1f / Engine.IterationsPerSecond;
            banana.AddCentralForce(PendingSwing.Power * PendingSwing.GlobalHitDirection / dt);
            var axis = banana.GlobalTransform.basis.Xform(Vector3.Up);
            if (axis.Dot(Vector3.Up) < 0f) axis *= -1;
            banana.AddTorque((PendingSwing.Spin / dt) * axis);
        }

        private void _OnBananaSleepingStateChanged()
        {
            if (State == HoleState.BananaFlying && Banana.Sleeping)
            {
                _ToAim();
            }
        }

        private void _TweenCameraToCourseStart()
        {
            var tween = GetTree().CreateTween();
            tween.SetEase(Tween.EaseType.Out).SetTrans(Tween.TransitionType.Expo);
            tween.TweenMethod(this, "_TweenCameraToTarget_Method", _cameraRig.Translation, Transform.Xform(Start.Translation), 1f);
        }

        private void _TweenCameraToBanana()
        {
            var tween = GetTree().CreateTween();
            tween.SetEase(Tween.EaseType.Out).SetTrans(Tween.TransitionType.Expo);
            tween.TweenMethod(this, "_TweenCameraToTarget_Method", _cameraRig.Translation, Transform.Xform(Banana.Translation), 1f);
        }

        private void _TweenCameraToTarget_Method(Vector3 trans)
        {
            _cameraRig.Translation = trans;
        }

        private void _SyncPivotAim()
        {
            ChimpPivot.Rotation = new Vector3(0f, -SwingControl.Swing.HorizontalAngle, 0f);
            var chimpPivotAxis = _localPlaySurfaceNormal.Cross(ChimpPivot.Transform.basis.Xform(Vector3.Up)).Normalized();
            if (Mathf.IsEqualApprox(chimpPivotAxis.Length(), 1f))
                ChimpPivot.Rotate(chimpPivotAxis, -_localPlaySurfaceNormal.SignedAngleTo(Vector3.Up, chimpPivotAxis));
        }
    
        // helper for timer
        private void _IncrementShots()
        {
            ShotsTaken +=1;
        }
    }

}