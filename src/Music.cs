using Godot;
using System;

public class Music : AudioStreamPlayer
{
    public void play_song(AudioStreamMP3 song)
    {
        Stop();
        Stream = song;
        Play(0f);
    }

    public void stop()
    {
        Stop();
    }
}
