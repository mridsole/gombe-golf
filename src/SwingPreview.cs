using System;
using Godot;
using System.Collections.Generic;

namespace gombegolf
{
    /// Uses little floating balls (floating bananas?) to preview the trajectory.
    /// This is not an accurate trajectory preview, it just captures the vibe of the shot
    /// The initial direction is correct, though.
    public class SwingPreview : Spatial
    {
        public SwingControl.SwingParams Swing;

        public PackedScene PreviewScene
        {
            get => _PreviewScene;
            set
            {
                _PreviewScene = value;
                _PopulatePreviewPrimitives();
            }
        }

        public int PreviewNodes
        {
            get => _PreviewNodes;
            set
            {
                _PreviewNodes = value;
                _PopulatePreviewPrimitives();
            }
        }
        
        public float Mass = 1f;

        private List<Spatial> _PreviewPrimitives = new List<Spatial>();
        private List<Vector3> _PreviewPrimAxes = new List<Vector3>();
        private PackedScene _PreviewScene = Resources.SwingPreviewPrimScene;
        private int _PreviewNodes = 6;
        private List<Vector3> _trajectoryPoints = new List<Vector3>();
        
        public override void _Ready()
        {
            _PopulatePreviewPrimitives();
        }

        public override void _PhysicsProcess(float dt)
        {
            _ComputeTrajectory();
            _DisplayComputedTrajectory();
            
            // Little rotation effect on the preview prims
            for (var i = 0; i < _PreviewNodes; i++)
            {
                _PreviewPrimitives[i].Rotate(_PreviewPrimAxes[i], 1f * dt);
            }
        }

        private void _ComputeTrajectory()
        {
            var gravity = 9.8f * this.GlobalTransform.basis.XformInv(Vector3.Down);
            var dt = 0.005f;
            
            // Use swing parameters to compute initial velocity.
            // TODO: Spin
            var initForce = Swing.Power * Swing.HitDirection;
            var initVel = initForce / Mass;
            
            // Compute the first 250ms of the trajectory.
            var pos = Vector3.Zero;
            var vel = initVel;
            _trajectoryPoints.Clear(); // TODO: Pool / don't realloc? Probably doesn't matter
            for (var i = 0; i < (int)(0.25f / dt); i++)
            {
                _trajectoryPoints.Add(pos);
                // TODO: RK4 instead of Euler...
                vel += dt * (gravity + Vector3.Up.Cross(vel) * Swing.Spin) / Mass;
                pos += dt * vel;
            }
        }

        // Position the primitives in accordance with the shot trajectory
        private void _DisplayComputedTrajectory()
        {
            var pointsPerPrim = Mathf.FloorToInt(_trajectoryPoints.Count / (float)_PreviewNodes);
            var j = 0;
            for (var i = 0; i < _trajectoryPoints.Count; i++)
            {
                if (i % pointsPerPrim == (pointsPerPrim - 1))
                {
                    var point = _trajectoryPoints[i];
                    var prim = _PreviewPrimitives[j];
                    prim.Translation = point;
                    j++;
                }
            }
        }

        private void _PopulatePreviewPrimitives()
        {
            // Free the last ones.
            foreach (var prim in _PreviewPrimitives) prim.QueueFree();
            _PreviewPrimitives.Clear();
            _PreviewPrimAxes.Clear();
            for (var i = 0; i < PreviewNodes; i++)
            {
                var inst = PreviewScene.Instance<Spatial>();
                _PreviewPrimitives.Add(inst);
                AddChild(inst);
                // Randomize the pose.
                var rand = new Random();
                var axis = new Vector3(rand.Next(0, 100), rand.Next(0, 100), rand.Next(0, 100)).Normalized();
               inst.Rotate(axis, rand.Next(0,359) / (2*Mathf.Pi));
               _PreviewPrimAxes.Add(new Vector3(rand.Next(0,100), rand.Next(0,100), rand.Next(0,100)).Normalized());
            }
        }
    }
}