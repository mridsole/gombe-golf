using Godot;
using System;

[Tool]
public class PodiumIslandScene : Spatial
{
    [Export]
    public bool ChimpsVisible
    {
        get => _ChimpsVisible;
        set
        {
            _ChimpsVisible = value;
            GetNode<Spatial>("Chimps").Visible = value;
        }
    }

    private bool _ChimpsVisible = false;
    
    public override void _Ready()
    {
        ChimpsVisible = _ChimpsVisible;
    }
}
