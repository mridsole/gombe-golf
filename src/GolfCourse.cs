using Godot;

namespace gombegolf
{
    /// The top-level logic for a course. Within a single scene.
    /// TODO: Currently just single-player. Local multiplayer support? Coloured bananas?
    public class GolfCourse : Spatial
    {
        private CameraRig _cameraRig;
        private Spatial _holes;
        private Spatial _course;
        private Spatial _bananaContainer;
        
        public override void _Ready()
        {
            _cameraRig = GetNode<CameraRig>("%CameraRig");
            _holes = GetNode<Spatial>("%Holes");
        }
    }
}