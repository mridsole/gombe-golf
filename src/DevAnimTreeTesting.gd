extends AnimationTree

var index = 0
var state_machine
onready var bone_attach = $"../Armature/Skeleton/BoneAttachment"

func _ready():
	state_machine = get("parameters/playback")

func play_animation(animation, item_index =-1, random_offset = 0):
	for node in bone_attach.get_children():
		node.visible = false
	if item_index >= 0:
		bone_attach.get_child(item_index).visible = true
	state_machine.travel(animation)
	if random_offset > 0:
		# well this doesnt do anything
		var offset = rand_range(0, random_offset)
		advance(offset)
