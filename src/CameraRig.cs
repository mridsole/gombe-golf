using Godot;
using System;
using Vector2 = Godot.Vector2;

namespace gombegolf
{

    [Tool]
    public class CameraRig : Spatial
    {
        public Camera Camera;

        [Export] public bool Active = true;
        [Export] public float ZoomStrength = 0.10f;
        [Export] public float Sensitivity = 0.003f;
        
        [Export] public float FreeCameraSensitivity = 0.0015f;
        [Export] public float FreeCameraSpeed = 20f;

        [Export] public float Distance = 1f;
        [Export] public float Latitude = Mathf.Pi / 2f;
        [Export] public float Longitude = 0f;
        

        private Vector2 _lastMousePos = new Vector2(-1f, -1f);

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            Camera = GetNode<Camera>("%Camera");
        }

        private void _ProcessInput(float dt)
        {
            var mousePos = GetViewport().GetMousePosition();

            if (Input.IsActionPressed("camera_free"))
            {
                if (Input.IsActionPressed("camera_free_forward")) Camera.Translation += FreeCameraSpeed * dt * Camera.Transform.basis.Xform(Vector3.Forward);
                if (Input.IsActionPressed("camera_free_back")) Camera.Translation -= FreeCameraSpeed * dt * Camera.Transform.basis.Xform(Vector3.Forward);
                if (Input.IsActionPressed("camera_free_left")) Camera.Translation += FreeCameraSpeed * dt * Camera.Transform.basis.Xform(Vector3.Left);
                if (Input.IsActionPressed("camera_free_right")) Camera.Translation -= FreeCameraSpeed * dt * Camera.Transform.basis.Xform(Vector3.Left);
                
                var mouseDelta = mousePos - _lastMousePos;
                Camera.Rotate(Vector3.Up, -FreeCameraSensitivity * mouseDelta.x);
                Camera.Rotate(Camera.Transform.basis.Xform(Vector3.Left), FreeCameraSensitivity * mouseDelta.y);
                
                // HACK: This Actually works...
                _lastMousePos = (GetViewport().Size / 2).Floor();
                Input.SetMouseMode(Input.MouseMode.Captured);
                Input.SetMouseMode(Input.MouseMode.Hidden);
            }
            else
            {
                if (Input.IsActionJustReleased("camera_free"))
                {
                    // TODO: Maybe tween the camera back out of free look?
                    Input.SetMouseMode(Input.MouseMode.Visible);
                }
                
                // Process inputs to update camera state (don't move camera yet)
                if (Input.IsActionJustReleased("camera_zoom_out")) Distance *= (1 + ZoomStrength);
                if (Input.IsActionJustReleased("camera_zoom_in")) Distance *= (1 - ZoomStrength);

                // Update latitude and longitude while dragging.
                if (Input.IsActionPressed("camera_drag"))
                {
                    var mouseDelta = mousePos - _lastMousePos;
                    Longitude += Sensitivity * mouseDelta.x;
                    Latitude += -Sensitivity * mouseDelta.y;
                }
                _lastMousePos = mousePos;
            }
        }

        public override void _Process(float dt)
        {
            // Only process camera input in the actual game.
            if (Active && !Engine.EditorHint) _ProcessInput(dt);
            
            // Hard bounds on latitude and distance.
            Latitude = Mathf.Clamp(Latitude, 0.1f * Mathf.Pi, 0.5F * Mathf.Pi);
            Distance = Mathf.Clamp(Distance, 1f, 50f);

            if (Engine.EditorHint || !Input.IsActionPressed("camera_free"))
            {
                Camera.Translation = new Vector3(
                    Distance * Mathf.Cos(Longitude) * Mathf.Sin(Latitude),
                    Distance * Mathf.Cos(Latitude),
                    Distance * Mathf.Sin(Longitude) * Mathf.Sin(Latitude)
                );
                
                Camera.LookAt(this.Transform.origin, this.Transform.basis.y);
            }
        }
    }

}