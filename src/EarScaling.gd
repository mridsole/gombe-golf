extends MeshInstance



func _ready():
	var pause_menu = get_node("/root/PauseMenu")
	pause_menu.get_node("%VolumeSlider").connect("value_changed", self, "set_ears")


func set_ears(value):
	self.set("blend_shapes/Key 1", 1-value)
