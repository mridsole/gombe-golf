using Godot;
using System;

namespace gombegolf
{

    public class PauseMenu : Control
    {
        public Button ResumeButton;
        public Button MainMenuButton;
        public Slider VolumeSlider;
        
        public bool Paused
        {
            get => _paused;
            set {
                if (value && !_paused) _OpenMenu();
                else if (!value && _paused) _CloseMenu();
            }
        }

        // Can't pause in the main menu, or in course review.
        public bool CanPause = true;

        private bool _paused = false;
        
        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            ResumeButton = GetNode<Button>("%ResumeButton");
            MainMenuButton = GetNode<Button>("%MainMenuButton");
            VolumeSlider = GetNode<Slider>("%VolumeSlider");

            ResumeButton.Connect("pressed", this, "_OnResumePressed");
            MainMenuButton.Connect("pressed", this, "_OnMainMenuPressed");
            VolumeSlider.Connect("value_changed", this, "_OnVolumeChanged");

            VolumeSlider.Value = 0.5f;
            
            _CloseMenu();
        }

        public override void _UnhandledInput(InputEvent ev)
        {
            if (ev.IsActionReleased("ui_cancel") && CanPause) Paused = !Paused;
        }

        private void _OpenMenu()
        {
            var db = AudioServer.GetBusVolumeDb(AudioServer.GetBusIndex("Master"));
            VolumeSlider.Value = GD.Db2Linear(db);
            
            GetTree().Paused = true;
            Visible = true;
            GrabFocus();
            _paused = true;
        }

        private void _CloseMenu()
        {
            GetTree().Paused = false;
            Visible = false;
            ReleaseFocus();
            _paused = false;
        }
        
        private void _OnResumePressed()
        {
            _CloseMenu();
        }

        private void _OnMainMenuPressed()
        {
            GetTree().ChangeSceneTo(Resources.MainMenuScene);
        }

        private void _OnVolumeChanged(float val)
        {
            AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex("Master"), GD.Linear2Db(val));
        }
    }
}