using Godot;
using System;

namespace gombegolf
{
    public class GombeEnvironment : Spatial
    {
        public override void _Ready()
        {
            var env = GetNode<WorldEnvironment>("WorldEnvironment");
            var light = GetNode<DirectionalLight>("DirectionalLight");
            // Change some parameters if we're using GLES2.
            if ((string)ProjectSettings.GetSetting("rendering/quality/driver/driver_name") == "GLES2")
            {
                env.Environment.AmbientLightEnergy = 1.77f;
                light.LightEnergy = 1.235f;
                light.LightIndirectEnergy = 1.204f;
            }
        }
    }
}