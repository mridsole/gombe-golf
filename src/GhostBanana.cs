using Godot;
using System;
using System.Drawing.Design;
using Array = Godot.Collections.Array;

namespace gombegolf
{
	public class GhostBanana : Banana
	{
		static private PackedScene BananaHitParticles = GD.Load<PackedScene>("res://assets/particles/Ghost_Banana_Hit.tscn");
	}
}