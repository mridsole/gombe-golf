using System;
using Godot;
using System.Collections.Generic;

namespace gombegolf
{
    /// A course consists of multiple holes.
    /// Node structure:
    ///     Holes: Spatial              - Each child should be a CourseHole. Courses are added in order.
    ///     EndingPodium: PodiumIslandScene   - Place it where u want the closing cutscene to be
    public class Course : Spatial
    {
        private Spatial _holesNode;
        private List<CourseHole> _holes = new List<CourseHole>();
        private CameraRig _cameraRig;

        public int ActiveHoleIndex = -1;

        public CourseHole ActiveHole => (ActiveHoleIndex == -1 || ActiveHoleIndex >= _holes.Count) ? null : _holes[ActiveHoleIndex];
        public List<int> Pars = new List<int>();
        public List<int> Scores = new List<int>();
        public Control HUD;
        public Control CourseReviewGUI;
        public PodiumIslandScene EndingPodium;

        private Music _music;
        private AudioStreamPlayer _chimpChorusPlayer;

        public override void _Ready()
        {
            _cameraRig = GetNode<CameraRig>("CameraRig");
            _holesNode = GetNode<Spatial>("Holes");
            foreach (var child in _holesNode.GetChildren())
            {
                var hole = child as CourseHole;
                _holes.Add(hole);
            }
            HUD = Resources.HUDScene.Instance<Control>();
            AddChild(HUD);
            HUD.MouseFilter = Control.MouseFilterEnum.Ignore;

            EndingPodium = GetNode<PodiumIslandScene>("EndingPodium");
            if (EndingPodium == null) throw new Exception("Course: Could not find child `EndingPodium`.");

            var pause_menu = GetNode<PauseMenu>("/root/PauseMenu");
            pause_menu.CanPause = true;

            _chimpChorusPlayer = new AudioStreamPlayer();
            AddChild(_chimpChorusPlayer);
            _music = GetNode<Music>("/root/Music");
        }

        public void Begin()
        {
            ActiveHoleIndex = -1;
            _NextHoleOrFinish();
        }

        public override void _Process(float delta)
        {
            // Update every frame lol
            if (ActiveHole != null)
            {
                // Update HUD
                HUD.GetNode<Label>("%HoleNumberLabel").Text = $"HOLE: {ActiveHoleIndex + 1}";
                HUD.GetNode<Label>("%ParLabel").Text = $"PAR: {ActiveHole.Par}";
                HUD.GetNode<Label>("%ShotsLabel").Text = $"SHOTS: {ActiveHole.ShotsTaken}";
                HUD.GetNode<ProgressBar>("%PowerBar").Value = 100f * ActiveHole.SwingControl.Swing.Power / SwingControl.MaxSwingPower;
                
                var spinBarL = HUD.GetNode<ProgressBar>("%SpinBarLeft");
                var spinBarR = HUD.GetNode<ProgressBar>("%SpinBarRight");
                // BIG HACK LOL - post-processing positioning after UI flow
                // We just want to reflect the left spin bar, so that the "progress" goes to the right!
                spinBarL.RectScale = new Vector2(-1, 1);
                spinBarL.RectPosition = new Vector2(spinBarL.RectSize.x, 0);
                var spin = 100f * ActiveHole.SwingControl.Swing.Spin / SwingControl.MaxSpin;
                if (spin >= 0f)
                {
                    spinBarL.Value = spin;
                    spinBarR.Value = 0f;
                }
                else
                {
                    spinBarL.Value = 0f;
                    spinBarR.Value = -spin;
                }
            }
        }

        private void _OnHoleComplete(object sender, CourseHole.HoleResult result)
        {
            ActiveHole.HoleComplete -= _OnHoleComplete;
            ActiveHole.End();
            Scores.Add(result.ShotsTaken);
            Pars.Add(ActiveHole.Par);
            _NextHoleOrFinish();
        }

        private void _NextHoleOrFinish()
        {
            ActiveHoleIndex += 1;
            if (ActiveHole != null)
            {
                ActiveHole.Begin(_cameraRig);
                ActiveHole.HoleComplete += _OnHoleComplete;
            }
            else
            {
                _FinishCourse();
            }
        }

        private void _FinishCourse()
        {
            var pause_menu = GetNode<PauseMenu>("/root/PauseMenu");
            pause_menu.CanPause = false;
            pause_menu.Paused = false;
            CourseReviewGUI = Resources.CourseReviewGUIScene.Instance<Control>();
            AddChild(CourseReviewGUI);
            _PopulateScorecard(CourseReviewGUI.GetNode<GridContainer>("%ScorecardGrid"));
            var finishButton = CourseReviewGUI.GetNode<Button>("%FinishButton");
            finishButton.Connect("pressed", this, "_OnFinishButtonPressed");
            
            EndingPodium.ChimpsVisible = true;
            HUD.Visible = false;
            _cameraRig.Translation = EndingPodium.Translation;
            _cameraRig.Active = false;
            var orbitTween = GetTree().CreateTween();
            // TODO: SetLoops doesn't actually do anything (even when given a number). Bug?
            orbitTween.SetEase(Tween.EaseType.InOut).SetTrans(Tween.TransitionType.Linear);
            orbitTween.TweenProperty(_cameraRig, "Longitude", _cameraRig.Longitude + 2 * Mathf.Pi, 4f);
            _cameraRig.Latitude = Mathf.Deg2Rad(65);
            _cameraRig.Distance = 20f;
            
            _chimpChorusPlayer.Stream = Resources.ChimpChorusSound;
            _chimpChorusPlayer.Seek(0);
            _chimpChorusPlayer.Play();
            _chimpChorusPlayer.VolumeDb = -7f;
            
            _music.play_song(Resources.GenralDrumsMusic);
        }

        private void _OnFinishButtonPressed()
        {
            // Return to the main menu.
            GetTree().ChangeSceneTo(Resources.MainMenuScene);
            QueueFree();
        }

        /// Populate a GridContainer with the scores of the course.
        private void _PopulateScorecard(GridContainer scorecard)
        {
            var N = Scores.Count;
            scorecard.Columns = N + 1;
            
            var holeLabel = new Label();
            holeLabel.Text = "HOLE";
            scorecard.AddChild(holeLabel);
            for (var i = 0; i < N; i++)
            {
                var holeI = new Label();
                holeI.Text = (i + 1).ToString();
                scorecard.AddChild(holeI);
            }
            
            var parLabel = new Label();
            parLabel.Text = "PAR";
            scorecard.AddChild(parLabel);
            for (var i = 0; i < N; i++)
            {
                var parI = new Label();
                parI.Text = Pars[i].ToString();
                scorecard.AddChild(parI);
            }
            
            var youLabel = new Label();
            youLabel.Text = "YOU";
            scorecard.AddChild(youLabel);
            for (var i = 0; i < N; i++)
            {
                var youI = new Label();
                youI.Text = Scores[i].ToString();
                // TODO: These dont work
                if (Scores[i] < Pars[i]) youI.AddColorOverride("font_color", new Color(0.4f, 1f, 0.4f));
                if (Scores[i] > Pars[i]) youI.AddColorOverride("font_color", new Color(1f, 0.4f, 0.4f));
                youI.AddColorOverride("font_color_shadow", new Color(0f, 0f, 0f));
                scorecard.AddChild(youI);
            }
        }
    }
}