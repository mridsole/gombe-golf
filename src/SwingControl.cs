using System;
using System.Collections.Generic;
using Godot;
    
namespace gombegolf
{
    
    /// Control for banana.
    /// TODO: How to visualise?
    public class SwingControl : Spatial
    {
        public static float MaxSwingPower = 40f;
        public static float MinSwingPower = 2f;

        public static float MaxSpin  = 8f;
        public static float MinSpin = -MaxSpin;

        static float MaxVerticalAngle = 0.5f * 0.5f * Mathf.Pi; // 45deg up
        static float MinVerticalAngle = 0.005f; // Forward

        static float GhostIdleTime = 0.5f;
        static float GhostHitTime = 1.0f;

        public class SwingConfirmedEventArgs : EventArgs { public SwingParams Swing; }
        public event EventHandler<SwingConfirmedEventArgs> SwingConfirmed;
        public event EventHandler GhostWakeupEvent;
        public event EventHandler<SwingConfirmedEventArgs> GhostHitEvent;
        public Timer GhostIdleTimer;
        public Timer GhostHitTimer;

        public struct SwingParams
        {
            public float HorizontalAngle;
            public float VerticalAngle;
            public float Power;
            public float Spin;
            
            public Vector3 GlobalHitDirection;

            public Vector3 HitDirection
            {
                get
                {
                    return new Vector3(
                        Mathf.Cos(HorizontalAngle) * Mathf.Cos(VerticalAngle),
                        Mathf.Sin(VerticalAngle), 
                        Mathf.Sin(HorizontalAngle) * Mathf.Cos(VerticalAngle)
                    );
                }
            }
        }
        
        public SwingParams Swing;

        private bool _deferredSwingConfirm = false;
        private bool _deferredGhostWakeupEvent = false;
        private bool _deferredGhostHitEvent = false;

        private SwingPreview _swingPreview = new SwingPreview();
        
        public SwingControl()
        {
            Swing = new SwingParams();
        }

        public override void _Ready()
        {
            Swing.Power = 10f; // Good default
            AddChild(_swingPreview);
            GhostHitTimer = new Timer();
            GhostHitTimer.Connect("timeout", this, "set", new Godot.Collections.Array { "_deferredGhostHitEvent", true} );
            AddChild(GhostHitTimer);

            GhostIdleTimer = new Timer();
            GhostIdleTimer.OneShot = true;
            GhostIdleTimer.Connect("timeout", this, "set", new Godot.Collections.Array { "_deferredGhostHitEvent", true });
            AddChild(GhostIdleTimer);
            GhostIdleTimer.Start(GhostIdleTime);
        }

        public override void _PhysicsProcess(float dt)
        {
            _ProcessInput();
            _swingPreview.Swing = Swing;

            if (_deferredSwingConfirm)
            {
                var args = new SwingConfirmedEventArgs();
                args.Swing = Swing;
                SwingConfirmed?.Invoke(this, args);
                _deferredSwingConfirm = false;
            }  else if (_deferredGhostHitEvent)
            {
                var args = new SwingConfirmedEventArgs();
                args.Swing = Swing;
                GhostHitEvent?.Invoke(this, args);
                _deferredGhostHitEvent = false;
                GhostHitTimer.Start(GhostHitTime);
            }
            else if (_deferredGhostWakeupEvent)
            {
                //NULL WARNING NULL ARGS WARNING
                GhostHitTimer.Stop();
                GhostWakeupEvent?.Invoke(this, null);
                _deferredGhostWakeupEvent = false;
                GhostIdleTimer.Start(GhostIdleTime);
            }
        }

        private void _ProcessInput()
        {
            // Don't process control if we are moving the camera freely.
            if (Input.IsActionPressed("camera_free")) return;

            var mod = 1.0f;
            if (Input.IsActionPressed("fine_control")) mod = 0.25f;
            var h_axis = Input.GetAxis("swing_aim_left", "swing_aim_right");
            var v_axis = Input.GetAxis("swing_aim_down", "swing_aim_up");
            var s_axis = Input.GetAxis("swing_spin_right", "swing_spin_left");
            var p_axis = Input.GetAxis("swing_power_less", "swing_power_more");

            Swing.HorizontalAngle += h_axis * 0.02f * mod;
            Swing.VerticalAngle += v_axis * 0.02f * mod;
            Swing.Spin += s_axis * 0.2f * mod;
            Swing.Power += p_axis * 0.1f * mod;

            // if input
            if (h_axis != 0 || v_axis != 0 || s_axis != 0 || s_axis != 0)
            {
                if (GhostIdleTimer.IsStopped()) _deferredGhostWakeupEvent = true;
                else GhostIdleTimer.Start(GhostIdleTime);
            }

            // Clamps
            Swing.VerticalAngle = Mathf.Clamp(Swing.VerticalAngle, MinVerticalAngle, MaxVerticalAngle);
            Swing.Power = Mathf.Clamp(Swing.Power, MinSwingPower, MaxSwingPower);
            Swing.Spin = Mathf.Clamp(Swing.Spin, MinSpin, MaxSpin);

            // The caller may not have access to SwingControl later (might not be in the scene) so save the global
            // direction as well.
            Swing.GlobalHitDirection = GlobalTransform.basis.Xform(Swing.HitDirection);

            // Fire off event. NOTE: Defer until after all processing.
            if (Input.IsActionJustPressed("swing_confirm"))
            {
                _deferredSwingConfirm = true;
            }
        }
    }
}