extends AnimationPlayer


onready var bone_attach = $"../Armature/Skeleton/BoneAttachment"

func _hide_everything(_value):
	for node in bone_attach.get_children():
		node.visible = false
