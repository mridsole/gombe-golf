using Godot;

namespace gombegolf
{
    /// Resources loaded at startup.
    public class Resources
    {
        public static PackedScene BananaScene = GD.Load<PackedScene>("res://assets/Banana.tscn");
        public static PackedScene GhostBananaScene = GD.Load<PackedScene>("res://assets/Ghost_Banana.tscn");
        public static PackedScene SwingPreviewPrimScene = GD.Load<PackedScene>("res://assets/SwingPreviewPrim.tscn");
        public static PackedScene ChimpScene = GD.Load<PackedScene>("res://assets/chimp.tscn");
        public static PackedScene WinConfettiScene = GD.Load<PackedScene>("res://assets/particles/confetti.tscn");
        public static PackedScene HUDScene = GD.Load<PackedScene>("res://scenes/GUI/HUD.tscn");
        public static PackedScene CourseReviewGUIScene = GD.Load<PackedScene>("res://scenes/GUI/CourseReview.tscn");
        public static PackedScene MainMenuScene = GD.Load<PackedScene>("res://scenes/GUI/main_menu_background.tscn");
        
        // Music
        public static AudioStreamMP3 AimMusic = GD.Load<AudioStreamMP3>("res://assets/sound/music/aim.mp3");
        public static AudioStreamMP3 BananaFlyingMusic = GD.Load<AudioStreamMP3>("res://assets/sound/music/banana_flying.mp3");
        public static AudioStreamMP3 GameStartChimpMusic = GD.Load<AudioStreamMP3>("res://assets/sound/music/game_start_chimp.mp3");
        public static AudioStreamMP3 GameStartRangaMusic = GD.Load<AudioStreamMP3>("res://assets/sound/music/game_start_ranga.mp3");
        public static AudioStreamMP3 MenuThemeMusic = GD.Load<AudioStreamMP3>("res://assets/sound/music/menu_theme.mp3");
        public static AudioStreamMP3 GenralDrumsMusic = GD.Load<AudioStreamMP3>("res://assets/sound/music/general_drums.mp3");
        // TODO: Actual win song
        public static AudioStreamMP3 WinMusic = GD.Load<AudioStreamMP3>("res://assets/sound/music/menu_theme.mp3");

        
        // Sound effects
        public static AudioStreamOGGVorbis Impact1Sound = GD.Load<AudioStreamOGGVorbis>("res://assets/sound/effects/impact.1.ogg");
        public static AudioStreamOGGVorbis Impact2Sound = GD.Load<AudioStreamOGGVorbis>("res://assets/sound/effects/impact.2.ogg");
        public static AudioStreamOGGVorbis Impact3Sound = GD.Load<AudioStreamOGGVorbis>("res://assets/sound/effects/impact.3.ogg");
        public static AudioStreamOGGVorbis Impact4Sound = GD.Load<AudioStreamOGGVorbis>("res://assets/sound/effects/impact.4.ogg");
        
        public static AudioStreamMP3 ChimpAngrySound = GD.Load<AudioStreamMP3>("res://assets/sound/music/chimp_angry.mp3");
        public static AudioStreamMP3 ChimpChorusSound = GD.Load<AudioStreamMP3>("res://assets/sound/music/chimp_chorus.mp3");
    }
}